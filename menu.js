import {config, version} from './config.js';
import _ from 'lodash';
import {shinobi} from './data.js';
import {composeErrorPage, msxUrl} from './utils.js';


const aboutPage = {
    pages: [{
        background: msxUrl('img/bg7.jpg'),
        items: [
            {
                layout: '0,0,12,4',
                color: 'msx-glass',
                headline: 'MSX Portal for Shinobi',
                text: `
                    {ico:msx-green:verified} {txt:msx-white:Version ${version}}{br}
                    {txt:msx-yellow:The way to bring your IP cameras to the TVs of all kinds and sizes!}{br}
                    Shinobi-MSX is a small server application that aggregates IP cameras from your Shinobi (open source cctv solution) instance and provides them as a content for Media Station X. This way, you can explore live streams (and recordings in the future) on any TV or tablet that is capable of running Media Station X.{br}
                    Made in Ukraine by Oleg Vivtash. Please visit the links below for more information.
                `,
                action: 'info:Greetings to Moe, lilBjo, GaLa V and HARM!'
            },
            {
                headline: 'Gitlab',
                text: 'project page',
                image: msxUrl('img/qr-gitlab.png'),
                imageFiller: 'height-right',
                action: 'link:window:https://gitlab.com/ovivtash/shinobi-msx',
                layout: '0,4,4,2',
            },            {
                headline: 'Shinobi',
                text: 'homepage',
                image: msxUrl('img/qr-shinobi.png'),
                imageFiller: 'height-right',
                action: 'link:window:https://shinobi.video/',
                layout: '4,4,4,2',
            },            {
                headline: "Oleg's",
                text: 'homepage',
                image: msxUrl('img/qr-oleg.png'),
                imageFiller: 'height-right',
                action: 'link:window:https://www.vivtash.net/',
                layout: '8,4,4,2',
            },
        ]
    }]
}

export function menuRoutes(fastify, options, done) {

    fastify.get('/menu.json', async (req, res) => {
        try {
            const channels = await shinobi.getTvChannels(config.shinobi.groupKey)
            const groupItems = _.chain(channels).map('groupTitle').uniq().reject(t => !t || t.length === 0).map(groupTitle => {
                return {
                    label: groupTitle,
                    icon: 'copy-all',
                    data: msxUrl('channels.json', {group: groupTitle})
                }
            }).value()

            let streamItems = [
                {
                    type: 'separator',
                    label: 'Live Streams'
                },
                {
                    label: 'All cameras',
                    icon: 'content-copy',
                    data: msxUrl('channels.json')
                },
                {
                    label: 'Ungrouped',
                    icon: 'list',
                    data: msxUrl('channels.json', {ungrouped: 1})
                },
                ...groupItems,
            ]

            if (_.size(groupItems) === 1) {
                streamItems = [{
                    label: 'Live streams',
                    icon: 'live-tv',
                    data: msxUrl('channels.json')
                }]
            }

            return {
                background: msxUrl('img/bg3.jpg'),
                logo: msxUrl('img/logo.png'),
                menu: [
                    ...streamItems,
                    {
                        type: 'separator',
                        display: false, // TODO
                    },
                    {
                        display: false, // TODO
                        icon: 'restore',
                        label: 'Recordings',
                        data: msxUrl('recordings/cameras.json')
                    },
                    {
                        type: 'separator',
                    },
                    {
                        label: 'About',
                        icon: 'info-outline',
                        data: aboutPage
                    },
                    {
                        type: 'settings',
                        icon: 'settings',
                        label: 'MSX Settings'
                    },
                ]
            }
        } catch (error) {
            return {
                headline: 'Error',
                menu: [
                    {
                        label: error.name,
                        data: composeErrorPage(error)
                    }
                ]
            }
        }
    })

    done()
}