import Fastify from 'fastify'
import * as cors from 'fastify-cors'
import * as serveStatic from 'fastify-static'
import {config, version} from "./config.js";
import {menuRoutes} from "./menu.js";
import {channelRoutes} from "./channels.js";
import path from "path";
import {_dirname} from "./utils.js";
import {recordingRoutes} from "./recordings.js";

const msx = Fastify({
    logger: true
})

msx.register(cors, {
    // put your options here
})
msx.register(serveStatic, {
    root: path.join(_dirname, 'public'),
    prefix: '/img/',
})

msx.register(menuRoutes)
msx.register(channelRoutes)
msx.register(recordingRoutes, {prefix: '/recordings'})

msx.get('/msx/start.json', async (req, res) => {
    return {
        "name": 'Shinobi MSX',
        "version": version,
        "parameter": "menu:{PREFIX}{SERVER}/menu.json",
        "welcome": "none"
    }
})


const start = async () => {
    try {
        await msx.listen(config.port, config.listen_ip)
    } catch (err) {
        msx.log.error(err)
        process.exit(1)
    }
}

start()