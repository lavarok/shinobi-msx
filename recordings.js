import {config} from "./config.js";
import _ from "lodash";
import {msxUrl} from "./utils.js";
import {shinobi} from "./data.js";

export function recordingRoutes(fastify, options, done) {

    fastify.get('/cameras.json', async (req, res) => {

        const cameras = [{name: 'Test'}]

        return {
            type: "list",
            headline: "Cameras",
            template: {
                layout: '0,0,4,3',
                imageFiller: 'width-center',
                type: "separate",
                icon: "msx-white-soft:live-tv",
                color: "msx-glass"
            },
            items: _.map(cameras, cam => {
                return {
                    title: cam.name,
                }
            })
        }
    })

    done()
}